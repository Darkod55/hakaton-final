
        <div class="row">
            <div class="col-md-9 col-md-offset-3 forposts">
            <?php
                    include_once __DIR__."/forcards.php";
                    $x = 0;
                    for($i = 0; $i < count($results); $i++) {
                        if($results[$i]['views'] > $x['views']) {
                            $x = $results[$i];
                        }
                    }?>                    
                   <div class="bigcard card<?= $x['id'] ?>">
                   <div class="col-md-6 col-xs-12 featuredimage" style="background-image: url('<?= $x['featured_image']; ?>')">
                        <p></p>
                    </div>
                   <div class="col-md-5 colfeatured">
                        <div class="forfeatured">
                            <div class="card-content featcon">
                                <p class="post-title featured"><?= $x['title']; ?></p>
                                <p class="post-content featured"><?= substr($x['content'], 0, 250); ?></p>
                                <button class="btn butaddcomp btnfeatured" style="background-color: <?= $x['color']; ?>" data-toggle="modal" data-target="#mod-card<?= $x['id'] ?>">Find Out More</button>
                                <i class="fal fa-share-alt" id="forshare"></i><p id="testfield">
                                <a href="http://www.facebook.com/sharer.php?u=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-facebook"></i></a>
                                <a href="https://plus.google.com/share?url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-google-plus"></i></a>
                                <a href="www.instagram.com"><i class="fab fa-instagram"></i></a>
                                <a href="http://reddit.com/submit?url=brainster-academy.mk/hakaton-final/&amp;title=Hakaton Final" target="_blank"><i class="fab fa-reddit"></i></a>
                                <a href="https://twitter.com/intent/tweet"><i class="fab fa-twitter-square"></i></a>
                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
        </div>