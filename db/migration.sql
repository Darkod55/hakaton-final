-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Oct 26, 2019 at 08:50 PM
-- Server version: 5.7.25
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `HakatonDarko`
--

-- --------------------------------------------------------

--
-- Table structure for table `AddCompany`
--

CREATE TABLE `AddCompany` (
  `comp_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `about` varchar(255) NOT NULL,
  `employee` smallint(6) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_approved` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `AddCompany`
--

INSERT INTO `AddCompany` (`comp_id`, `name`, `website`, `about`, `employee`, `email`, `is_approved`) VALUES
(1, 'adsdacs', 'dsadda.com', 'dawscvava', 1, 'darkod55@yahoo.com', 1),
(6, 'Darko\'s Company', 'onlinemastery.co.uk', 'This is a test company', 0, 'darkod55@yahoo.com', 1),
(7, 'Darko\'s Company 2', 'onlinema222stery.co.uk', 'This is a test company 2', 1, 'darkod55@yahoo.com', 0),
(10, 'dada', 'saac', 'caca', 1, 'mikemunevar@gmail.com', 1),
(14, 'dsasdsd', 'dsadasd', 'dasdasd', 1, 'mikemunevar@gmail.com', 1),
(15, 'dsasdsd', 'dsadasd', 'dasdasd', 1, 'mikemunevar@gmail.com', 1),
(16, 'Test', 'Test', 'Test', 1, 'darkod55@yahoo.com', 1),
(17, 'Stamp', 'onlinema222stery.co.uk', 'dsadada', 1, 'slobodankaanonova11@yahoo.com', 0),
(18, 'Datalites', 'test.com', 'Ova e kontent test', 1, 'test@test.com', 0),
(19, 'Test', 'Teeees.com', 'fadcvas av', 1, 'test@test.com', 0),
(21, 'EEEEE', 'dda.com', 'cascscacac', 0, 'test@test.com', 0),
(22, 'wow', 'asfdasca', 'wwqsda', 0, 'test@test.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Categories`
--

CREATE TABLE `Categories` (
  `cat_id` int(11) NOT NULL,
  `title_cat` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Categories`
--

INSERT INTO `Categories` (`cat_id`, `title_cat`, `color`) VALUES
(1, 'HR EDUCATION', '#EFE4E3'),
(2, 'RETAIN TALENT', '#D7A89F'),
(3, 'REMOTE TEAMS', '#BA5147'),
(4, 'PM TOOLS', '#F06B5E'),
(5, 'ATTRACT TALENT', '#CDECE5'),
(6, 'COMMUNICATION TOOLS', '#8FC0B6'),
(7, 'GLOBAL COMMUNICATION', '#78A199'),
(8, 'HIRING', '#2D3D39');

-- --------------------------------------------------------

--
-- Table structure for table `PostContent`
--

CREATE TABLE `PostContent` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `featured_image` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `views` int(11) NOT NULL DEFAULT '0',
  `date_creation` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `PostContent`
--

INSERT INTO `PostContent` (`id`, `title`, `featured_image`, `content`, `category_id`, `views`, `date_creation`) VALUES
(12, 'Post 1 HR', 'assets/images/HR/How to ensure.jpg', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an un', 1, 7, '2019-10-07 00:38:13'),
(13, 'Post 2 HR', 'assets/images/HR/Know Your Team is the answer.jpeg', 'Content for Post 2', 1, 121, '2019-10-07 00:38:10'),
(14, 'Post 3 HR', 'assets/images/HR/Meet HR buddies in flash.jpeg', 'Content for Post 3', 1, 142, '2019-10-07 00:38:12'),
(15, 'Post 4 HR', 'assets/images/HR/Meet your HR Competitors.jpeg', 'Content for Post 4', 1, 18, '2019-10-07 00:38:12'),
(16, 'Post 5 HR', 'assets/images/HR/What does Team Culture Mean.jpg', 'Content for Post 5', 1, 159, '2019-10-07 00:31:13'),
(17, 'Post 6 HR', 'assets/images/HR/Why are greater levels of trust and commitment important.jpg', 'Content for Post 6', 1, 0, '2019-10-07 00:35:13'),
(26, 'Post 1 Remote Teams', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 3, 1, '2019-10-07 00:32:13'),
(27, 'Post 2 Remote Teams', 'assets/images/Remote-Teams/Challenges of building a great remote environment.jpeg', 'Content for Post 2', 3, 0, '2019-10-07 00:33:13'),
(28, 'Post 3 Remote Teams', 'assets/images/Remote-Teams/You don’t need everyone physically together to create a strong culture.jpeg', 'Content for Post 3', 3, 1, '2019-10-07 00:31:13'),
(29, 'Post 4 Remote Teams', 'assets/images/Remote-Teams/Cherry-pick the best people for the job from all around the world.jpeg', 'Content for Post 4', 3, 0, '2019-10-07 00:36:13'),
(30, 'Post 5 Remote Teams', 'assets/images/Remote-Teams/Digital Transformation of the company.jpg', 'Content for Post 5', 3, 0, '2019-10-07 00:32:13'),
(31, 'Post 8 Remote Teams', 'assets/images/Remote-Teams/Higher productivity from better work-life balance.jpg', 'Content for Post 6', 3, 0, '2019-10-07 00:33:13'),
(32, 'Post 6 Remote Teams', 'assets/images/Remote-Teams/Hire better not quicker.jpeg', 'Content for Post 7', 3, 0, '2019-10-07 00:36:13'),
(33, 'Post 7 Remote Teams', 'assets/images/Remote-Teams/How_to_build_social_connection_in_a_remote_team.jpeg', 'Content for Post 8', 3, 0, '2019-10-07 00:28:13'),
(34, 'Post 1 Attract Talent', 'assets/images/Attract-Talent/2Price_is_what_you_pay_Value_is_what_you get..jpg', 'Content for Post 1', 5, 0, '2019-10-07 00:18:13'),
(35, 'Post 2 Attract Talent', 'assets/images/Attract-Talent/Become an expert in creating experts.jpg', 'Content for Post 2', 5, 1, '2019-10-07 00:28:13'),
(36, 'Post 3 Attract Talent', 'assets/images/Attract-Talent/Price is what you pay. Value is what you get..jpg', 'Content for Post 3', 5, 1, '2019-10-07 00:28:13'),
(37, 'Post 4 Attract Talent', 'assets/images/Attract-Talent/Use Onboarding to build trust with new employees.jpg', 'Content for Post 4', 5, 1, '2019-10-07 00:32:13'),
(38, 'Post 5 Attract Talent', 'assets/images/Attract-Talent/What makes a company worth working in it.jpg', 'Content for Post 5', 5, 0, '2019-10-07 00:38:10'),
(39, 'Post 6 Attract Talent', 'assets/images/Attract-Talent/Your approach to hiring is all wrong.jpg', 'Content for Post 6', 5, 0, '2019-10-07 00:34:13'),
(40, 'Post 1 Retain Talent', 'assets/images/Retain-Talent/Correlation between talent and company growth.jpg', 'Content for Post 1', 2, 0, '2019-10-07 00:38:13'),
(41, 'Post 2 Retain Talent', 'assets/images/Retain-Talent/What does nurturing your employees really mean.jpg', 'Content for Post 2', 2, 0, '2019-10-07 00:38:13'),
(42, 'Post 3 Retain Talent', 'assets/images/Retain-Talent/What to do to avoid employee turnover.jpg', 'Content for Post 3', 2, 0, '2019-10-07 00:38:13'),
(43, 'Post 4 Retain Talent', 'assets/images/Retain-Talent/What to do to avoid employee turnover2.jpg', 'Content for Post 4', 2, 0, '2019-10-07 00:38:13'),
(44, 'Post 5 Retain Talent', 'assets/images/Retain-Talent/Why are your employees leaving.jpg', 'Content for Post 5', 2, 2, '2019-10-07 00:38:13'),
(45, 'Post 6 Retain Talent', 'assets/images/Retain-Talent/Why is employer branding important.jpg', 'Content for Post 6', 2, 1, '2019-10-07 00:38:13'),
(56, 'Post 1 PM Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 4, 0, '2019-10-11 18:08:59'),
(57, 'Post 2 PM Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 2', 4, 0, '2019-10-11 18:08:59'),
(58, 'Post 3 PM Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 3', 4, 0, '2019-10-11 18:08:59'),
(59, 'Post 1 Attract Talent', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 5, 0, '2019-10-11 18:08:59'),
(60, 'Post 2 Attract Talent', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 2', 5, 0, '2019-10-11 18:08:59'),
(61, 'Post 3 Attract Talent', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 3', 5, 0, '2019-10-11 18:08:59'),
(62, 'Post 1 Communication Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 6, 0, '2019-10-11 18:08:59'),
(63, 'Post 2 Communication Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 2', 6, 0, '2019-10-11 18:08:59'),
(64, 'Post 3 Communication Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 3', 6, 0, '2019-10-11 18:08:59'),
(65, 'Post 4 Communication Tools', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 4', 6, 0, '2019-10-11 18:08:59'),
(66, 'Post 1 Global Communication', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 7, 0, '2019-10-11 18:11:32'),
(67, 'Post 2 Global Communication', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 2', 7, 0, '2019-10-11 18:11:32'),
(68, 'Post 3 Global Communication', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 3', 7, 1, '2019-10-11 18:11:32'),
(69, 'Post 4 Global Communication', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 4', 7, 0, '2019-10-11 18:11:32'),
(70, 'Post 5 Global Communication', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 5', 7, 0, '2019-10-11 18:11:32'),
(71, 'Post 1 Hiring', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 1', 8, 0, '2019-10-11 18:11:32'),
(72, 'Post 2 Hiring', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 2', 8, 0, '2019-10-11 18:11:32'),
(73, 'Post 3 Hiring', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 3', 8, 1, '2019-10-11 18:11:32'),
(74, 'Post 4 Hiring', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 4', 8, 1, '2019-10-11 18:11:32'),
(75, 'Post 5 Hiring', 'assets/images/Remote-Teams/buying_business.jpg', 'Content for Post 5', 8, 0, '2019-10-11 18:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `Subscribe`
--

CREATE TABLE `Subscribe` (
  `SubID` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Unsubscribed` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Subscribe`
--

INSERT INTO `Subscribe` (`SubID`, `Email`, `Unsubscribed`) VALUES
(1, 'darkod55@yahoo.com', 0),
(2, 'darkodimitrovski95@gmail.com', 0),
(3, 'slobodankaanonova11@yahoo.com', 0),
(4, 'test@test.com', 0),
(5, 'test@test.com', 0),
(6, 'test@test.com', 0),
(7, 'darkod55@yahoo.com', 0),
(8, 'darkod55@yahoo.com', 0),
(9, 'slobodankaanonova11@yahoo.com', 0),
(10, 'darkodimitrovski95@gmail.com', 0),
(11, 'teest@test.com', 0),
(12, 'darkod55@yahoo.com', 0);

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

CREATE TABLE `Users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Users`
--

INSERT INTO `Users` (`id`, `username`, `password`) VALUES
(1, 'darko', 'darko'),
(2, 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `AddCompany`
--
ALTER TABLE `AddCompany`
  ADD UNIQUE KEY `Company_ID` (`comp_id`);

--
-- Indexes for table `Categories`
--
ALTER TABLE `Categories`
  ADD UNIQUE KEY `id` (`cat_id`);

--
-- Indexes for table `PostContent`
--
ALTER TABLE `PostContent`
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `views` (`views`);

--
-- Indexes for table `Subscribe`
--
ALTER TABLE `Subscribe`
  ADD UNIQUE KEY `SubID` (`SubID`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `AddCompany`
--
ALTER TABLE `AddCompany`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `Categories`
--
ALTER TABLE `Categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `PostContent`
--
ALTER TABLE `PostContent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `Subscribe`
--
ALTER TABLE `Subscribe`
  MODIFY `SubID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `Users`
--
ALTER TABLE `Users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `PostContent`
--
ALTER TABLE `PostContent`
  ADD CONSTRAINT `Category_id` FOREIGN KEY (`category_id`) REFERENCES `Categories` (`cat_id`);
