$("#subscribe_mail").on("click", function(e) {
    e.preventDefault();
    let feedback=$('#mail-feedback'),inputField=$("#subscribe");
    let mail = inputField.val().toLowerCase();
    feedback.show().html('<img src="assets/images/loader/code-loader-42px.gif">');
  
    ValidateEmail(mail,feedback,inputField);
  
    updateMailFeedback(feedback);
  });
  
  $(".posts-btn").on("click", function(e) {
      e.preventDefault();
      let feedback=$("#posts-feedback"),inputField=$(".mailPosts");
      let mail = inputField.val().toLowerCase();
      feedback.show().html('<img src="assets/images/loader/code-loader-42px.gif">');
    
      ValidateEmail(mail,feedback,inputField);
    
      updateMailFeedback(feedback);
    });
  
  $("#emailAddCompany").on("blur", function() {
      let mail = $("#emailAddCompany")
        .val()
        .toLowerCase();
      $("#emailAddCompany-feedback")
        .show()
        .html('<img src="assets/images/loader/code-loader-42px.gif">');
      // console.log(mail);
      let mailformat = new RegExp("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$");
      if (!mailformat.test(mail)){
          $('#emailAddCompany-feedback').css('color','red');
          $("#emailAddCompany-feedback").html('&nbsp;&nbsp;Неправилна форма на маил адреса.');
          $("#emailAddCompany").val('');
      } else {
          $("#emailAddCompany-feedback").html('');
      }
    });
  
  
  function ValidateEmail(inputText,feedback,inputField) {
    var mailformat = new RegExp("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$");
  
    if (mailformat.test(inputText)) {
      $.ajax({
        type: "POST",
        url: "app/api/api_checkMail.php",
        data: { subscriber: inputText },
        success: function(data) {
          feedback.css('color','black').html(data);
          inputField.val("");
        }
      });
    } else {
      feedback.css('color','red').html("&nbsp;&nbsp;Внесовте неправилна маил адреса");
      inputField.val("");
    }
  }
  
  function updateMailFeedback(feedback) {
    setTimeout(function() {
      feedback.fadeOut("slow");
    }, 3000);
  }
  