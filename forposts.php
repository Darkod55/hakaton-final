<div class="row">
            <div class="col-md-9 col-md-offset-3 forposts flex postwrapper" >
                <?php
                    include_once __DIR__."/forcards.php";
                    foreach($results as $row) { ?>
                    <div class="card single-card cat<?= $row['cat_id'] ?>" id="postcard<?= $row['id'] ?>" date-cre="<?= $row['date_creation'] ?>" data-catid="cat<?= $row['cat_id'] ?>" data-id="<?= $row['id'] ?>" data-toggle="modal" data-target="#mod-card<?= $row['id'] ?>" style="background-image: url('<?= $row['featured_image']; ?>')">
                        <div class="for-opacity">
                            <div class="card-content">
                                <p class="post-title">
                                    <?= $row['title']; ?>
                                </p>
                                <p class="post-cat">
                                    <?= $row['title_cat']; ?>
                                </p>
                                <hr style="background-color: <?= $row['color']; ?>" class="hr pull-left">
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="mod-card<?= $row['id'] ?>" tabindex="-1" role="dialog" aria-labelledby="<?= $row['title'] ?>">
                        <div class="modal-dialog model-cards" role="document">
                            <div class="modal-content">
                                <div class="modal-body modal-size">
                                    <button type="button" class="close close-per" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="sidenav sidemodal">
                                    <p class="post-title">
                                        <?= $row['title']; ?>
                                    </p>
                                    <div class="for-cat">
                                        <p class="post-cat post-title-modal">
                                            <?= $row['title_cat']; ?>
                                        </p>
                                        <hr style="background-color: <?= $row['color']; ?>" class="hr hr-modal pull-left">
                                    </div>
                                </div>
                                <div class="container-fluid inmodal">
                                    <div class="row">
                                        <div class="col-md-9 col-md-offset-3 formodal">
                                            <div class="forimage-test">
                                                <img class="cat-image img img-resposnive" src="<?= $row['featured_image']; ?>" />
                                                <div class="testbg">
                                                    <img class="imagefull" src="<?= $row['featured_image']; ?>" />
                                                </div>
                                            </div>
                                            <div class="for-content-post">
                                                <p class="post-title">
                                                    <?= $row['title']; ?>
                                                </p>
                                                <div class="social">
                                                    <i class="fal fa-share-alt" id="sharemodal"></i>
                                                    <p id="sharebuttons">                  
                                                        <a href="http://www.facebook.com/sharer.php?u=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-facebook"></i></a>
                                                        <a href="https://plus.google.com/share?url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-google-plus"></i></a>
                                                        <a href="www.instagram.com"><i class="fab fa-instagram"></i></a>
                                                        <a href="http://reddit.com/submit?url=brainster-academy.mk/hakaton-final/&amp;title=Hakaton Final" target="_blank"><i class="fab fa-reddit"></i></a>
                                                        <a href="https://twitter.com/intent/tweet"><i class="fab fa-twitter-square"></i></a>
                                                        <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=brainster-academy.mk/hakaton-final/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                                                    </p>     
                                                </div>                                                
                                                <p class="post-content">
                                                    <?= $row['content']; ?>
                                                </p>
                                                <form class="navbar-form form-in-modal" action="app/api/api_subscribe.php">
                                                    <div class="form-group custom-form-group">
                                                        <div class="form-group flex custom-form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon custom-form-icon"><i class="far fa-envelope"></i></span>
                                                                <!-- <input type="email" name="email" required class="form-control custom-form" placeholder="Get two new looks every week" aria-describedby="basic-addon1"> -->
                                                                <input type="email" name="email" required class="form-control custom-form mailPosts" placeholder="Get two new looks every week" aria-describedby="basic-addon1">
                                                            </div>
                                                            <!-- <button class="btn submitBtn" type="submit" value="Submit">Stay Updated -->
                                                            <button class="btn submitBtn posts-btn" type="submit" value="Submit">Stay Updated
                                                            </button>
                                                            </button>
                                                        </div>
                                                        <span id="posts-feedback"></span>
                                                    </div>
                                                    <?php
                                                        require_once __DIR__.'/functions.php';
                                                        if(isset($_GET['error'])) {
                                                            echo errorMessage();
                                                        }
                                                        if(isset($_GET['success']) && $_GET['success'] == 'true') {
                                                            echo successMessage();
                                                        }
                                                    ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php  }?>
            <div class="row">
                <div class="col-md-12 forlaika">
                    <div class="card single-card cat8" id="postadd" data-catid="cat8">
                        <div class="card-contents">
                            <p class="post-plus">+</p>
                            <p class="post-add">ADD NEW COMPANY</p>
                        </div>
                    </div>
                    <div class="card single-card cat8" id="postadd1" data-catid="cat8">
                            <a class="linklaika" href="https://www.wearelaika.com/" target="blank"><p class="forlaika"> </p></a>
                    </div>
                </div>
            </div>
            </div>
        </div>