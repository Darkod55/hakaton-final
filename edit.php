<?php

require_once __DIR__."/db/mypdo.php";

if(isset($_REQUEST)) {   
    $id = $_REQUEST['id'];
    $db = new MyPDO();
    $sql = "SELECT * FROM AddCompany WHERE comp_id = :id";
    $sh = $db->prepare($sql);
    $sh->bindParam(":id",$id);
    $sh->execute(); 
    $results = $sh->fetchAll();
}
?>
<html>
    <head>
        <link href="assets/css/project.css" rel="stylesheet">
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <div class="container editform">
            <div class="row">
                <div class="col-md-12">
                    <p>Company ID - <?php echo $results[0]['comp_id']; ?> </p>
                    <p>Company Name - <?php echo $results[0]['name']; ?> </p>
                    <p>Company Website - <?php echo $results[0]['website']; ?> </p>
                    <p>About - <?php echo $results[0]['about']; ?> </p>
                    <p>Company Email - <?php echo $results[0]['email']; ?> </p>
                    <form action="foredit.php" method="POST">
                        <input type="hidden" name="comp_id" value="<?php echo $results[0]['comp_id']; ?>"/>
                        <label>Approve Company</label> <br />
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary radiobtn">
                                <input class="radio-inline" type="radio" name="is_approved" value="1" checked>Yes
                            </label>
                            <label class="btn btn-primary radiobtn">
                                <input class="radio-inline" type="radio" name="is_approved" value="0">No
                            </label>
                        </div> <br />
                    <button class="btn btn-default butaddcomp" type="submit" name="submit" value="edit user">Submit</button>
                    </form>
                </div>
            </div>
        </div>
        <script src="assets/js/jquery-3.4.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
    </body>
</html>
        
