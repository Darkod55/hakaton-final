<?php
function successMessage() {
    if(isset($_REQUEST['success'])) {
        if($_REQUEST['success'] == 'true') {
            echo "<p class='success'>Thank you for subscribing</p>";
        }
        elseif($_REQUEST['success'] == 'login') {
            echo "<p class='success'>Welcome admin</p>";
        }
        elseif($_REQUEST['success'] == 'admin') {
            return "<p class='cat-item item-login'><a id='dashboard' href='dashboard.php'>Dashboard</a></p>";
        }
        elseif($_REQUEST['success'] == 'updated') {
            return "<p class='success'>Company status updated</p>";
        }
        elseif($_REQUEST['success'] == 'deleted') {
            echo "<p class='success'>Company successfully deleted.</p>";
        }
        elseif($_REQUEST['success'] == 'addedcomp') {
            echo "<p class='success'>Company Successfully added.</p>";
        }
    }
}

function errorMessage() {
    if(isset($_REQUEST['error'])) {
        if($_REQUEST['error'] == 'wronguser') {
            echo "<p class='success'>This is wrong user</p>";
        }
        elseif($_REQUEST['error'] == 'not-admin') {
            echo "<p class='success'>This user is not admin</p>";
        }
        elseif($_REQUEST['error'] == 'deleted') {
            echo "<p class='success'>Company successfully deleted.</p>";
        }
    }
}
?>