<?php

class Search {
    public $search;
    private $db;
    public function __construct($search, MyPDO $db)
    {
        $this->search = $search;
        $this->db = $db;
    }


    public function search() {
        $sql = "SELECT * FROM PostContent WHERE title LIKE :search";
        $args = ["search" => $this->search];
        $this->db->run($sql, $args);
    }

}




?>