<?php

class AddCompany {
    public $id;
    public $name;
    public $website;
    public $about;
    public $employee;
    public $email;
    public $is_approved;
    private $db;

    public function __construct($name, $website, $about, $employee, $email, $is_approved, MyPDO $db)
    {
        // $this->id = $id;
        $this->name = $name;
        $this->website = $website;
        $this->about = $about;
        $this->employee = $employee;
        $this->email = $email;
        $this->is_approved = $is_approved;
        $this->db = $db;
    }

    public function save() {
        $sql = "INSERT INTO AddCompany (name, website, about, employee, email) VALUES (:name, :website, :about, :employee, :email)";
        $args = ["name" => $this->name, "website" => $this->website, 
        "about" => $this->about, "employee" => $this->employee, "email" => $this->email];
        $this->db->run($sql, $args);
    }

    public function update() {
        $sql = "UPDATE AddCompany SET `is_approved` = :is_approved WHERE id = id";
    }

    public function delete() {
        $sql = "DELETE";
    }

    public function fetchById() {
        $sql = "SELECT FROM";
    }

    public function fetchByCategory() {
        $sql = "SELECT FROM";
    }

    public function fetchByDate() {
        $sql = "SELECT FROM";
    }

    public function fetchByPopularity() {
        $sql = "SELECT FROM";
    }
 
}




?>