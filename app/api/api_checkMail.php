<?php
    require_once __DIR__."/../../db/mypdo.php";

    if( (isset($_POST['subscriber']))){

        try{
            $subscriber=strtolower($_POST['subscriber']);
            // if(!empty($userCode)){
                $sql="SELECT Email FROM Subscribe";
                $db = new MyPDO();
                $st=$db->run($sql);
                $result=$st->fetchAll();

                $mailInSystem=false;
                for ($i=0;$i<count($result);$i++){
                    if($result[$i]['Email']==$subscriber){
                        $mailInSystem=true;
                    }
                }
                if($mailInSystem){
                    echo "&nbsp;&nbsp;{$subscriber} веќе постои во системот ";
                } else {
                    header("Location: api_subscribe.php?email={$subscriber}");
                }
                
        } catch (PDOException $e) {
            echo "Error: ".$e->getMessage();
            header('Location: index.php');
        }

    }
?>
