<?php 

require_once __DIR__."/../../db/mypdo.php";
require_once __DIR__."/../domain/addCompany.php";

if(isset($_REQUEST)) {   
    // $id = $_REQUEST['comp_id'];
    $name = $_REQUEST['name'];
    $website = $_REQUEST['website'];
    $about = $_REQUEST['about'];
    $employee = $_REQUEST['employee'];
    $email = $_REQUEST['email'];
    $is_approved = 0;

    $db = new MyPDO();
    $sub = new AddCompany($name, $website, $about, $employee, $email, $is_approved, $db);

    $sub->save();
    header("Location: ../../index.php?success=addedcomp");
    die();
}
?>