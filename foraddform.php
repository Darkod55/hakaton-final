<div class="row">
            <div class="col-md-9 col-md-offset-3" id="forform">
                <form class="addcompform"action="app/api/api_addcompany.php" method="POST">
                <button type="button" class="formclose close close-per" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="form-group">
                        <label>Add your company here</label>
                        <input type="text" name="name" class="form-control margin-bottom" id="name" placeholder="Company Name">
                        <input type="text" name="website" class="form-control margin-bottom" id="website" placeholder="Company Website">
                        <textarea rows="4" cols="50" name="about" class="form-control margin-bottom" id="about" placeholder="About company"></textarea>
                        <label for="employee" class="control-label">Do you work for this company?</label><br />
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary radiobtn">
                                <input class="radio-inline" type="radio" name="employee" value="1" checked>Yes
                            </label>
                            <label class="btn btn-primary radiobtn">
                                <input class="radio-inline" type="radio" name="employee" value="0">No
                            </label>
                        </div>
                    </div>
                        <!-- <label for="email">Your email adress</label>
                        <input type="email" name="email" class="form-control margin-bottom" id="email" placeholder="examle@example.com"> -->
                        <label for="email">Your email adress</label><span id='emailAddCompany-feedback'></span>
                        <input type="email" name="email" class="form-control margin-bottom" id="emailAddCompany" placeholder="example@example.com">
                    <button type="submit" class="btn btn-default butaddcomp" id="fortest">Submit</button>
                </form>
            </div>
        </div>